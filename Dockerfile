# Build Stage
FROM golang:1.18-alpine as build-stage

WORKDIR /app

COPY . ./

RUN go mod download && go mod verify

RUN go build -o /be-kelompok-2

# Deployment Stage
FROM alpine

COPY --from=build-stage /be-kelompok-2 /be-kelompok-2

COPY .env ./
COPY templates ./templates

# ENV PORT=3000
CMD [ "/be-kelompok-2" ]
